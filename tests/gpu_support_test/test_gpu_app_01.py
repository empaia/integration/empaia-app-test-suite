from sample_apps.sample_apps.test.gpu._GPU01_gpu_support_test_app import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_GPU01_gpu_support_test_app",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-gpu_test_app_01:v3.0",
    "path": "./sample_apps/sample_apps/test/gpu/_GPU01_gpu_support_test_app/v3",
}


EXPECTED_OUTPUTS = {"gpu_detected": True, "gpu_device": "GPU"}


def test_gpu_app_01():
    ead = app.ead
    run_eats_with_app(APP, ead, expected_outputs=EXPECTED_OUTPUTS)
