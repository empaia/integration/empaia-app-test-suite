from sample_apps.sample_apps.test.internal._ITA07_large_input_collection import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_ITA07_large_input_collection",
    "docker_tag": (
        "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-internal_test_app_07:v3.0"
    ),
    "path": "./sample_apps/sample_apps/test/internal/_ITA07_large_input_collection/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_ITA07_large_input_collection/eats_inputs",
}


def test_internal_app_07():
    ead = app.ead
    run_eats_with_app(APP, ead)
