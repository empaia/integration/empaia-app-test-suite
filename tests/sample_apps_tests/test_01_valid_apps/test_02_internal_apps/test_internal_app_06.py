from sample_apps.sample_apps.test.internal._ITA06_nested_large_collections import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_ITA06_nested_large_collections",
    "docker_tag": (
        "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-internal_test_app_06:v3.0"
    ),
    "path": "./sample_apps/sample_apps/test/internal/_ITA06_nested_large_collections/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_ITA06_nested_large_collections/eats_inputs",
}


def test_internal_app_06():
    ead = app.ead
    run_eats_with_app(APP, ead)
    # run twice to test functionality:
    # if providing inputs with an id, they shall not be reposted iff their data did not change
    run_eats_with_app(APP, ead)
