import pytest

from sample_apps.sample_apps.test.internal._ITA08_fluorescence_test_app import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_ITA08_fluorescence_test_app",
    "docker_tag": (
        "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-internal_test_app_08:v3.0"
    ),
    "path": "./sample_apps/sample_apps/test/internal/_ITA08_fluorescence_test_app/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_ITA08_fluorescence_test_app/eats_inputs",
}


@pytest.mark.skip(reason="ITA08 should be revised")
def test_internal_app_08():
    ead = app.ead
    run_eats_with_app(APP, ead)
