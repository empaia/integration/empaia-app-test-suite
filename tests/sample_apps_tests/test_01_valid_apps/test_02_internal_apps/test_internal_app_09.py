from sample_apps.sample_apps.test.internal._ITA09_order_collection_items import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_ITA09_order_collection_items",
    "docker_tag": (
        "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-internal_test_app_09:v3.0"
    ),
    "path": "./sample_apps/sample_apps/test/internal/_ITA09_order_collection_items/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_ITA09_order_collection_items/eats_inputs",
}


def test_internal_app_09():
    ead = app.ead
    run_eats_with_app(APP, ead)
