from sample_apps.sample_apps.test.internal._ITA10_configuration_tests import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_ITA10_configuration_tests",
    "docker_tag": (
        "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-internal_test_app_10:v3.0"
    ),
    "path": "./sample_apps/sample_apps/test/internal/_ITA10_configuration_tests/v3",
    "global_config": (
        "./sample_apps/sample_apps/test/internal/_ITA10_configuration_tests"
        "/v3/eats_inputs/configuration/configuration.json"
    ),
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_ITA10_configuration_tests/eats_inputs",
}


def test_internal_app_10():
    ead = app.ead
    run_eats_with_app(APP, ead)
