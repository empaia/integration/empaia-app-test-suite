from sample_apps.sample_apps.tutorial._TA03_annotation_results import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_TA03_annotation_results",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_03:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA03_annotation_results/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA03_annotation_results/eats_inputs",
}


def test_tutorial_app_03():
    ead = app.ead
    run_eats_with_app(APP, ead)
