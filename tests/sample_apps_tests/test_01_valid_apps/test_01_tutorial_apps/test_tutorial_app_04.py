from sample_apps.sample_apps.tutorial._TA04_output_references_output import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_TA04_output_references_output",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_04:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA04_output_references_output/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA04_output_references_output/eats_inputs",
}


def test_tutorial_app_04():
    ead = app.ead
    run_eats_with_app(APP, ead)
