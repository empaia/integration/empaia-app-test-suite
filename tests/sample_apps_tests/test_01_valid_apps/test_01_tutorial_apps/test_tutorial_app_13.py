from sample_apps.sample_apps.tutorial._TA13_pixelmap import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP_PREPROCESSING = {
    "name": "_TA13_pixelmap",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_13:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA13_pixelmap/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA13_pixelmap/eats_inputs",
}


def test_tutorial_app_13():
    ead = app.ead
    run_eats_with_app(APP_PREPROCESSING, ead, job_mode="preprocessing")
