from sample_apps.sample_apps.tutorial._TA05_classes import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_TA05_classes",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_05:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA05_classes/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA05_classes/eats_inputs",
}


def test_tutorial_app_05():
    ead = app.ead
    run_eats_with_app(APP, ead)
    # run twice to test functionality:
    # if providing inputs with an id, they shall not be reposted if their data did not change
    run_eats_with_app(APP, ead)
