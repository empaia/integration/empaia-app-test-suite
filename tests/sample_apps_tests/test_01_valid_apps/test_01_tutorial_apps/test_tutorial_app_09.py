from sample_apps.sample_apps.tutorial._TA09_additional_inputs import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_TA09_additional_inputs",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_09:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA09_additional_inputs/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA09_additional_inputs/eats_inputs",
}


def test_tutorial_app_09():
    ead = app.ead
    run_eats_with_app(APP, ead)
