from sample_apps.sample_apps.tutorial._TA10_app_with_ui import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP_UI_CONFIG_PATH = "sample_apps/sample_apps/tutorial/_TA10_app_with_ui/v3/app_ui_config.json"
APP = {
    "name": "_TA10_app_with_ui",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_10:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA10_app_with_ui/v3",
    "app_ui_url": "http://host.docker.internal:8888/sample-app-ui",
    "app_ui_config_file": APP_UI_CONFIG_PATH,
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA10_app_with_ui/eats_inputs",
}
APP_SMALL_ROI = {
    "name": "_TA10_app_with_ui",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_10:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA10_app_with_ui/v3",
    "app_ui_url": "http://host.docker.internal:8888/sample-app-ui",
    "app_ui_config_file": APP_UI_CONFIG_PATH,
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA10_app_with_ui/eats_inputs_small_roi",
}


def test_tutorial_app_10():
    ead = app.ead
    run_eats_with_app(APP, ead)

    # small roi
    run_eats_with_app(APP_SMALL_ROI, ead)
