from sample_apps.sample_apps.tutorial._TA08_failure_endpoint import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP_valid = {
    "name": "_TA08_failure_endpoint",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_08:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA08_failure_endpoint/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA08_failure_endpoint/valid_inputs",
}
APP_invalid = {
    "name": "_TA08_failure_endpoint",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_08:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA08_failure_endpoint/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA08_failure_endpoint/invalid_inputs",
}


def test_tutorial_app_08():
    ead = app.ead
    expected_error_message = "Input rectangle must have width greater than 1024"

    # should run without error
    run_eats_with_app(APP_valid, ead)
    # run twice to test functionality:
    # if providing inputs with an id, they shall not be reposted iff their data did not change
    run_eats_with_app(APP_valid, ead)

    # should run with error
    run_eats_with_app(APP_invalid, ead, expected_error=expected_error_message)
    # run twice to test functionality:
    # if providing inputs with an id, they shall not be reposted iff their data did not change
    run_eats_with_app(APP_invalid, ead, expected_error=expected_error_message)
