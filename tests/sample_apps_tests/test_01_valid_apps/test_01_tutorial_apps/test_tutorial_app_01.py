from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_TA01_simple_app",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_01:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA01_simple_app/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA01_simple_app/eats_inputs",
}


def test_tutorial_app_01():
    ead = app.ead
    run_eats_with_app(APP, ead)
    # run twice to test functionality:
    # if providing inputs with an id, they shall not be reposted iff their data did not change
    run_eats_with_app(APP, ead)
