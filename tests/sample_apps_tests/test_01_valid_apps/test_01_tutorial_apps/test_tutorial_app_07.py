from sample_apps.sample_apps.tutorial._TA07_configuration import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_TA07_configuration",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_07:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA07_configuration/v3",
    "global_config": (
        "./sample_apps/sample_apps/tutorial/_TA07_configuration/v3/eats_inputs/configuration/configuration.json"
    ),
    "customer_config": (
        "./sample_apps/sample_apps/tutorial/_TA07_configuration"
        "/v3/eats_inputs/configuration/customer_configuration.json"
    ),
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA07_configuration/eats_inputs",
}


def test_tutorial_app_07():
    ead = app.ead
    run_eats_with_app(APP, ead, always_register=True)
