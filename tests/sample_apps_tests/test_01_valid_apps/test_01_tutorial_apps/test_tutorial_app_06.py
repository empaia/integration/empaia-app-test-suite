from sample_apps.sample_apps.tutorial._TA06_large_collections import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_TA06_large_collections",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_06:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA06_large_collections/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA06_large_collections/eats_inputs",
}


def test_tutorial_app_06():
    ead = app.ead

    # 1st run should pass
    run_eats_with_app(APP, ead)
