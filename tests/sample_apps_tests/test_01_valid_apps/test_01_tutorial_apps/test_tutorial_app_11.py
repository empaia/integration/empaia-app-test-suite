from sample_apps.sample_apps.tutorial._TA11_preprocessing import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP_PREPROCESSING = {
    "name": "_TA11_preprocessing",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_11:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA11_preprocessing/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA11_preprocessing/eats_inputs_preprocessing",
}

APP_STANDALONE = {
    "name": "_TA11_preprocessing",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_11:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA11_preprocessing/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA11_preprocessing/eats_inputs_standalone",
}


def test_tutorial_app_11():
    ead = app.ead
    run_eats_with_app(APP_PREPROCESSING, ead, job_mode="preprocessing")

    run_eats_with_app(APP_STANDALONE, ead, job_mode="standalone")
