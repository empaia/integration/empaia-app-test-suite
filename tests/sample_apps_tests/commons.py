import json
import os
import shutil
import subprocess
from pathlib import Path
from time import sleep

import docker
import requests

from tests.print_utils import color, print_bold


def str_assert_exclude_whitespace(test_str, output_str):
    test_str = "".join(str(test_str).split())
    output_str = "".join(str(output_str).split())
    assert test_str in output_str


def get_app_list():
    lines = run_subprocess("eats apps list").split("\n")
    apps = []
    for line in lines:
        if line.startswith(" APP ID"):
            continue
        if line == "":
            continue
        else:
            split = [s for s in line.strip(" ").split(" ") if s != ""]
            app = {
                "APP ID": split[0],
                "EAD SHORT NAME": split[1],
                "EAD NAMESPACE": split[2],
                "DOCKER IMAGE": split[3],
                "CREATED": f"{split[4]} {split[5]}",
            }
            apps.append(app)

    return apps


def run_subprocess(cmd: str):
    try:
        output = subprocess.check_output(
            cmd, stderr=subprocess.STDOUT, shell=True, timeout=600, universal_newlines=True
        )
        return output
    except subprocess.CalledProcessError as e:
        print(e.output)
        raise e


def build_app_image(app_name: str, dockerfile_path: str, app_script: str = "app.py") -> str:
    tag = f"eats_test_{app_name.lower()}"

    def build():
        client = docker.from_env()
        client.images.build(path=dockerfile_path, tag=tag, buildargs={"app_script": app_script})

    build()
    return tag


def run_eats_with_app(
    app: dict,
    ead: dict,
    job_mode: str = "standalone",
    build: bool = False,
    always_register: bool = False,
    expected_error: str = None,
    expected_outputs: dict = None,
):
    sleep(1)
    parent_output_folder = Path("./tests/sample_apps_tests/tmp_outputs")
    parent_output_folder.mkdir(parents=True, exist_ok=True)
    output_folder = Path(parent_output_folder, app["name"])

    # delete output dir if exists
    try:
        shutil.rmtree(output_folder)
    except FileNotFoundError:
        pass

    # recreate output dir
    Path(output_folder).mkdir(parents=True, exist_ok=True)

    print_bold(f"\nRunning {app['name']}")

    # register app
    ead_file = os.path.join(app["path"], "ead.json")
    app_env = f"{parent_output_folder}/app.env"
    print(f"\N{bullet} Register app {app['name']}" + color.PURPLE)

    # check if app already registered
    app_found = False
    if not always_register:
        apps = get_app_list()
        for a in apps:
            if a["EAD NAMESPACE"] == ead["namespace"]:
                app_data = f"APP_ID={a['APP ID']}"
                app_found = True
                run_subprocess(f"echo {app_data} > {app_env}")
                break

    if not app_found:
        tag = None
        if build:
            print(f"\N{bullet} Build app image {app['name']}" + color.PURPLE)
            tag = build_app_image(app["name"], app["path"])
        tag = tag if tag else app["docker_tag"]
        command = f"eats apps register {ead_file} {tag}"
        if "global_config" in app:
            global_config_file = app["global_config"]
            command += f" --global-config-file {global_config_file}"
        if "customer_config" in app:
            customer_config_file = app["customer_config"]
            command += f" --customer-config-file {customer_config_file}"
        if "app_ui_url" in app:
            app_ui_url = app["app_ui_url"]
            command += f" --app-ui-url {app_ui_url}"
        if "app_ui_config_file" in app:
            app_ui_config_file = app["app_ui_config_file"]
            command += f" --app-ui-config-file {app_ui_config_file}"
        run_subprocess(f"{command} > {app_env}")
    print(color.END, end="\r")

    # read app_id
    with open(app_env, encoding="utf-8") as f:
        app_id = f.read().replace("APP_ID=", "").replace("\n", "")

    # detect input folder
    input_folder = f"{app['path'].rstrip('/')}/eats_inputs"
    if "input" in app:
        input_folder = app["input"]

    print(f"\N{bullet} Register and run job for app {app['name']}" + color.PURPLE)

    # exec job
    job_env = f"{parent_output_folder}/job.env"
    command = f"eats jobs exec {app_id} {input_folder} {output_folder} --job-mode {job_mode}"
    try:
        run_subprocess(f"{command} > {job_env}")
        print(color.END, end="\r")
    except subprocess.CalledProcessError as e:
        if not expected_error:
            raise e

        with open(job_env, encoding="utf-8") as f:
            job_id = f.read().split("\n")[0].replace("EMPAIA_JOB_ID=", "")

        client = docker.from_env()
        container = client.containers.get("nginx")
        internal_port = 80
        container_info = client.api.port(container.id, internal_port)
        host_port = container_info[0]["HostPort"]
        mds_url = f"http://127.0.0.1:{host_port}/mds-api"

        r = requests.get(f"{mds_url}/v3/jobs/{job_id}")
        r.raise_for_status()
        job = r.json()
        status = job["status"]
        error_message = job["error_message"]

        assert status == "ERROR"
        assert error_message == expected_error

    if not expected_error:
        # check outputs written
        output_keys = ead["modes"][job_mode]["outputs"]
        for output in output_keys:
            assert Path(output_folder, f"{output}.json").exists()
            if expected_outputs:
                with open(Path(output_folder, f"{output}.json"), encoding="utf-8") as f:
                    data = json.load(f)
                    if isinstance(expected_outputs[output], str):
                        assert data["value"].startswith(expected_outputs[output])
                    elif isinstance(expected_outputs[output], bool):
                        assert data["value"] == expected_outputs[output]
                    # add more types if needed
