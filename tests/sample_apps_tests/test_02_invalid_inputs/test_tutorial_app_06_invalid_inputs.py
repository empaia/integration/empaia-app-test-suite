import subprocess

from sample_apps.sample_apps.tutorial._TA06_large_collections import v3 as _TA06_large_collections_v3
from tests.sample_apps_tests.commons import run_eats_with_app, str_assert_exclude_whitespace

APP = {
    "name": "_TA06_large_collections",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_06:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA06_large_collections/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA06_large_collections/eats_inputs",
}
APP_inputs_changed = {
    "name": "_TA06_large_collections",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_06:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA06_large_collections/v3",
    "input": "./sample_apps/sample_apps/test/eats/invalid_inputs/_TA06_large_collections/inputs_with_id_changed",
}
APP_num_col_items_changed = {
    "name": "_TA06_large_collections",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_06:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA06_large_collections/v3",
    "input": (
        "./sample_apps/sample_apps/test/eats/invalid_inputs/"
        "_TA06_large_collections/inputs_with_id_num_col_items_changed"
    ),
}


def test_tutorial_app_06_invalid():
    ead = _TA06_large_collections_v3.ead

    # 1st run should pass
    run_eats_with_app(APP, ead)

    # 2nd run should NOT pass
    try:
        run_eats_with_app(APP_inputs_changed, ead)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = (
            "Value of [name] of input parameter [my_rectangles] with [id] "
            "[1b20ca52-61cc-4ddb-b32d-9cea4ffb089a] changed. If [my_rectangles] "
            "is a (nested) collection, this might also be an item of this collection. "
            "Delete volumes of the EMPAIA Test Suite <docker volume rm $(eats services volumes)>. "
            "Or if you want to preserve old job data, assign new IDs to the collection "
            "[my_rectangles] and to all of its (nested) items."
        )
        str_assert_exclude_whitespace(test_str, e.output)

    # 3nd run should NOT pass
    try:
        run_eats_with_app(APP_num_col_items_changed, ead)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = (
            "Number of items in collection [my_rectangles] changed. Delete volumes "
            "of the EMPAIA Test Suite <docker volume rm $(eats services volumes)>. "
            "Or if you want to preserve old job data, assign new IDs to the collection "
            "[my_rectangles] and to all of its (nested) items."
        )
        str_assert_exclude_whitespace(test_str, e.output)
