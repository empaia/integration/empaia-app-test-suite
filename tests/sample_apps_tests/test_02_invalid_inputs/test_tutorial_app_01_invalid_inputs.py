import subprocess

from sample_apps.sample_apps.tutorial._TA01_simple_app import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app, str_assert_exclude_whitespace

APP_MISSING_REF_ID = {
    "name": "_TA01_simple_app",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_01:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA01_simple_app/v3",
    "input": "./sample_apps/sample_apps/test/eats/invalid_inputs/_TA01_simple_app/missing_id_to_reference",
}
APP_MISSING_INPUT = {
    "name": "_TA01_simple_app",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_01:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA01_simple_app/v3",
    "input": "./sample_apps/sample_apps/test/eats/invalid_inputs/_TA01_simple_app/missing_input_file",
}
APP_NO_ANNOT_NAME = {
    "name": "_TA01_simple_app",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_01:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA01_simple_app/v3",
    "input": "./sample_apps/sample_apps/test/eats/invalid_inputs/_TA01_simple_app/no_name_input",
}
APP_NO_REF_TYPE = {
    "name": "_TA01_simple_app",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_01:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA01_simple_app/v3",
    "input": "./sample_apps/sample_apps/test/eats/invalid_inputs/_TA01_simple_app/no_reference_type_input",
}


def test_tutorial_app_01_invalid():
    ead = app.ead
    try:
        run_eats_with_app(APP_MISSING_REF_ID, ead)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = (
            "Could not register the following inputs: ['my_rectangle']. "
            "Check if their [reference_id]s are valid [id]s of other inputs."
        )
        str_assert_exclude_whitespace(test_str, e.output)

    try:
        run_eats_with_app(APP_MISSING_INPUT, ead)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = "Validating [my_rectangle] failed: No input file found"
        str_assert_exclude_whitespace(test_str, e.output)

    try:
        run_eats_with_app(APP_NO_ANNOT_NAME, ead)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = "Validating [my_rectangle] failed"
        str_assert_exclude_whitespace(test_str, e.output)

        test_str = "name"
        str_assert_exclude_whitespace(test_str, e.output)

        test_str = "Field required"
        str_assert_exclude_whitespace(test_str, e.output)

    try:
        run_eats_with_app(APP_NO_REF_TYPE, ead)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = "Validating [my_rectangle] failed"
        str_assert_exclude_whitespace(test_str, e.output)

        test_str = "reference_type"
        str_assert_exclude_whitespace(test_str, e.output)

        test_str = "Field required"
        str_assert_exclude_whitespace(test_str, e.output)
