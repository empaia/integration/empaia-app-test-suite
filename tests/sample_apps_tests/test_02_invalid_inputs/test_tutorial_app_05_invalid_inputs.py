import subprocess

from sample_apps.sample_apps.tutorial._TA05_classes import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app, str_assert_exclude_whitespace

APP_MISSING_ROIS = {
    "name": "_TA05_classes",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_05:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA05_classes/v3",
    "input": "./sample_apps/sample_apps/test/eats/invalid_inputs/_TA05_classes/missing_rois",
}


def test_tutorial_app_05_invalid():
    ead = app.ead
    try:
        run_eats_with_app(APP_MISSING_ROIS, ead)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = "ValidationError: Class constraint for annotation not fullfilled"
        str_assert_exclude_whitespace(test_str, e.output)
