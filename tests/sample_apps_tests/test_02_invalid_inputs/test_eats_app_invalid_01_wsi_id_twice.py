import subprocess

from sample_apps.sample_apps.test.eats.invalid_apps._IEA01_wsi_id_used_twice import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app

APP = {
    "name": "_IEA01_wsi_id_used_twice",
    "path": "./sample_apps/sample_apps/test/eats/invalid_apps/_IEA01_wsi_id_used_twice/v3",
}


def test_eats_app_invalid_01():
    ead = app.ead
    try:
        run_eats_with_app(APP, ead, build=True)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        print(e.output)
