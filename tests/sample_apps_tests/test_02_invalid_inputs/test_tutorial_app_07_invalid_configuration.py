import subprocess

from sample_apps.sample_apps.tutorial._TA07_configuration import v3 as app
from tests.sample_apps_tests.commons import run_eats_with_app, str_assert_exclude_whitespace

APP_MISSING_CONFIG = {
    "name": "_TA07_configuration",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_07:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA07_configuration/v3",
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA07_configuration/eats_inputs",
}
APP_EMPTY_CONFIG = {
    "name": "_TA07_configuration",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_07:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA07_configuration/v3",
    "global_config": (
        "./sample_apps/sample_apps/test/eats/invalid_apps/_IEA02_configuration_invalid"
        "/v3/eats_inputs/configuration_empty.json"
    ),
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA07_configuration/eats_inputs",
}
APP_MISSING_KEY = {
    "name": "_TA07_configuration",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_07:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA07_configuration/v3",
    "global_config": (
        "./sample_apps/sample_apps/test/eats/invalid_apps/_IEA02_configuration_invalid"
        "/v3/eats_inputs/configuration_missing_key.json"
    ),
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA07_configuration/eats_inputs",
}
APP_UNKNOWN_KEY = {
    "name": "_TA07_configuration",
    "docker_tag": "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_07:v3.0",
    "path": "./sample_apps/sample_apps/tutorial/_TA07_configuration/v3",
    "global_config": (
        "./sample_apps/sample_apps/test/eats/invalid_apps/_IEA02_configuration_invalid"
        "/v3/eats_inputs/configuration_unknown_key.json"
    ),
    "input": "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA07_configuration/eats_inputs",
}


def test_tutorial_app_07_invalid():
    ead = app.ead
    error_message = "App terminated before finalizing its computation"

    run_eats_with_app(APP_MISSING_CONFIG, ead, always_register=True, expected_error=error_message)

    try:
        run_eats_with_app(APP_EMPTY_CONFIG, ead, always_register=True)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = "Validation of global configuration failed"
        str_assert_exclude_whitespace(test_str, e.output)

    try:
        run_eats_with_app(APP_MISSING_KEY, ead, always_register=True)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = "Validation of global configuration failed"
        str_assert_exclude_whitespace(test_str, e.output)

    try:
        run_eats_with_app(APP_UNKNOWN_KEY, ead, always_register=True)
        raise AssertionError(" CalledProcessError expected !")
    except subprocess.CalledProcessError as e:
        test_str = "Validation of global configuration failed"
        str_assert_exclude_whitespace(test_str, e.output)
