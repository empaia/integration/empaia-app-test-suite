import functools


class color:
    PURPLE = "\033[95m"
    CYAN = "\033[96m"
    DARKCYAN = "\033[36m"
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    RED = "\033[91m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    END = "\033[0m"


def print_bold(text):
    print(color.BOLD + text + color.END)


def print_bullet(text, replace=False):
    end = ""
    if replace:
        end = "\r"
    print(f"\N{bullet} {text}", end=end)


def print_ok(text):
    print(color.GREEN + color.BOLD + "\N{check mark} " + color.END + text)


def print_failed(text):
    print(color.RED + color.BOLD + "\N{aegean check mark} " + color.END + text)


def print_step(text):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            print_bullet(text, replace=True)
            try:
                result = func(*args, **kwargs)
                print_ok(text)
                return result
            except Exception:
                print_failed(text)
                raise

        return wrapper

    return decorator
