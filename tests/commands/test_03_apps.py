from typer.testing import CliRunner

from empaia_app_test_suite.cli import app as eats
from tests.commands.commons import ServicesUpTest

runner = CliRunner()


class TestAppsCommand(ServicesUpTest):
    def test_apps_register_without_args(self):
        result = runner.invoke(eats, ["apps", "register"])
        assert "Usage: eats apps register [OPTIONS] EAD_FILE DOCKER_IMAGE" in result.stdout

    def test_apps_register_with_non_existent_ead_file(self):
        result = runner.invoke(
            eats, ["apps", "register", "some/non-existent/ead.json", "apps-register/test:non-existent"]
        )
        assert result.exit_code != 0

    # def test_apps_register_with_existing_but_invalid_ead_file(self):
    #     result = runner.invoke(
    #         eats,
    #         [
    #             "apps",
    #             "register",
    #             "sample_apps/sample_apps/invalid/solution_store/_001_invalid_type/ead.json",
    #             "apps-register/test:invalid",
    #         ],
    #     )
    #     assert result.exit_code != 0

    #     result = runner.invoke(eats, ["apps", "list"])
    #     assert result.exit_code == 0
    #     assert "app " not in result.stdout

    def test_apps_register_with_valid_ead_file(self):
        result = runner.invoke(
            eats,
            [
                "apps",
                "register",
                "sample_apps/sample_apps/tutorial/_TA01_simple_app/v3/ead.json",
                self.dummy_docker_image,
            ],
        )
        assert result.exit_code == 0

        result = runner.invoke(eats, ["apps", "list"])
        assert result.exit_code == 0
        assert "TA01v3" in result.stdout

    def test_apps_register_with_valid_ead_and_config_file(self):
        result = runner.invoke(
            eats,
            [
                "apps",
                "register",
                "--global-config-file",
                "sample_apps/sample_apps/tutorial/_TA07_configuration/v3/eats_inputs/configuration/configuration.json",
                "sample_apps/sample_apps/tutorial/_TA07_configuration/v3/ead.json",
                self.dummy_docker_image,
            ],
        )
        assert result.exit_code == 0

        result = runner.invoke(eats, ["apps", "list"])
        assert result.exit_code == 0
        assert "TA07v3" in result.stdout

    def test_apps_register_with_valid_ead_file_and_app_ui_url(self):
        result = runner.invoke(
            eats,
            [
                "apps",
                "register",
                "--app-ui-url",
                "http://localhost:4300",
                "sample_apps/sample_apps/tutorial/_TA10_app_with_ui/v3/ead.json",
                self.dummy_docker_image,
            ],
        )
        assert result.exit_code == 0

        result = runner.invoke(eats, ["apps", "list"])
        assert result.exit_code == 0
        assert "TA10v3" in result.stdout
