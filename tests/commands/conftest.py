from pathlib import Path

import pytest


def pytest_addoption(parser):
    parser.addoption("--mount-point", action="store", default=None)


def validate_and_resolve_mount_point(mount_point):
    assert mount_point is not None
    mount_point_path = Path(mount_point).resolve()
    return str(mount_point_path)


def pytest_configure(config):
    mount_point = config.getoption("--mount-point")
    pytest.mount_point = validate_and_resolve_mount_point(mount_point)
