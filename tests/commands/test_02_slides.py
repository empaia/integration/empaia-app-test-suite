from typer.testing import CliRunner

from empaia_app_test_suite.cli import app as eats
from tests.commands.commons import ServicesUpTest

runner = CliRunner()


class TestSlidesCommand(ServicesUpTest):
    def test_slides_register_without_args(self):
        result = runner.invoke(eats, ["slides", "register"])
        assert "Usage: eats slides register [OPTIONS] SLIDE_FILE" in result.stdout

    def test_slides_register_with_non_existent_file(self):
        result = runner.invoke(eats, ["slides", "register", "some/non/existent/slide.json"])
        assert result.exit_code != 0

    def test_slides_register_same_id_different_paths(self):
        result = runner.invoke(
            eats,
            [
                "slides",
                "register",
                (
                    "./sample_apps/sample_apps/test/eats/invalid_apps/"
                    "_IEA01_wsi_id_used_twice/v3/eats_inputs/my_wsi_1.json"
                ),
            ],
        )
        assert result.exit_code == 0
        result = runner.invoke(
            eats,
            [
                "slides",
                "register",
                (
                    "./sample_apps/sample_apps/test/eats/invalid_apps/"
                    "_IEA01_wsi_id_used_twice/v3/eats_inputs/my_wsi_2.json"
                ),
            ],
        )
        assert result.exit_code != 0

    def test_slides_register_valid_single_slide_file(self):
        result = runner.invoke(
            eats,
            [
                "slides",
                "register",
                "./sample_apps/sample_apps/test/eats/valid_test_inputs/_TA01_simple_app/eats_inputs/my_wsi.json",
            ],
        )
        assert result.exit_code == 0

    def test_slides_register_valid_collection_slide_file(self):
        result = runner.invoke(
            eats,
            [
                "slides",
                "register",
                (
                    "./sample_apps/sample_apps/test/eats/valid_test_inputs/_ITA05_input_templates"
                    "/eats_inputs/collection_wsi.json"
                ),
            ],
        )
        assert result.exit_code == 0

    def test_slides_register_valid_collection_slide_file_no_ids(self):
        result = runner.invoke(
            eats,
            [
                "slides",
                "register",
                (
                    "./sample_apps/sample_apps/test/eats/valid_test_inputs/_ITA05_input_templates"
                    "/eats_inputs/collection_wsi_no_ids.json"
                ),
            ],
        )
        assert result.exit_code == 0

    def test_slides_list(self):
        self.test_slides_register_valid_single_slide_file()
        result = runner.invoke(
            eats,
            ["slides", "list"],
        )
        assert result.exit_code == 0
        assert "3e86a882-bb3d-4d2f-b31d-c5704ba3efc1" in result.stdout
