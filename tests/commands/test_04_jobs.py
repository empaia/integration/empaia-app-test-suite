import os
import re
import uuid

from typer.testing import CliRunner

from empaia_app_test_suite.cli import app as eats
from tests.commands.commons import ServicesUpTest
from tests.sample_apps_tests.commons import build_app_image

runner = CliRunner()


class TestJobsCommand(ServicesUpTest):
    _app_base_path = "sample_apps/sample_apps/tutorial/_TA01_simple_app/v3"
    _ead = os.path.join(_app_base_path, "ead.json")
    _inputs = "sample_apps/sample_apps/test/eats/valid_test_inputs/_TA01_simple_app/eats_inputs"

    def _register_valid_app(self, docker_image=None):
        if not docker_image:
            docker_image = self.dummy_docker_image
        result = runner.invoke(
            eats,
            [
                "apps",
                "register",
                self._ead,
                docker_image,
            ],
        )
        assert result.exit_code == 0
        (app_id,) = re.findall("^APP_ID=(.+)$", result.stdout, re.MULTILINE)
        assert app_id
        return app_id

    def _register_valid_job(self, tmp_path=None, app_id=None):
        if not app_id:
            app_id = self._register_valid_app()
        result = runner.invoke(eats, ["jobs", "register", app_id, self._inputs])
        assert result.exit_code == 0
        assert "EMPAIA_JOB_ID=" in result.stdout
        assert "EMPAIA_TOKEN=" in result.stdout
        assert "EMPAIA_APP_API=" in result.stdout
        if tmp_path:
            job_env_str = ""
            for line in result.stdout.splitlines():
                if "=" in line:
                    job_env_str += line + "\n"
            path_to_job_env = os.path.join(tmp_path, "job.env")
            with open(path_to_job_env, "w", encoding="utf-8") as f:
                f.write(job_env_str)
            (job_id,) = re.findall("^EMPAIA_JOB_ID=(.+)$", job_env_str, re.MULTILINE)
            return job_id, path_to_job_env

    def test_jobs_register_without_args(self):
        result = runner.invoke(eats, ["jobs", "register"])
        assert "Usage: eats jobs register [OPTIONS] APP_ID INPUT_DIR" in result.stdout

    def test_jobs_register_with_invalid_app_id(self):
        result = runner.invoke(eats, ["jobs", "register", "invalid-app-id", "inputs"])
        assert result.exit_code != 0

    def test_jobs_register_with_valid_app_id(self):
        self._register_valid_job()

    def test_jobs_list_after_register(self, tmp_path):
        job_id, _ = self._register_valid_job(tmp_path=tmp_path)
        result = runner.invoke(eats, ["jobs", "list"])
        assert job_id in result.stdout
        assert "ASSEMBLY" in result.stdout

    def test_jobs_abort(self, tmp_path):
        job_id = str(uuid.uuid4())
        result = runner.invoke(eats, ["jobs", "abort", job_id])
        assert result.exit_code != 0
        job_id, _ = self._register_valid_job(tmp_path=tmp_path)
        result = runner.invoke(eats, ["jobs", "abort", job_id])
        assert result.exit_code != 0
        docker_image = build_app_image("simple_app", self._app_base_path)
        app_id = self._register_valid_app(docker_image=docker_image)
        job_id, path_to_job_env = self._register_valid_job(tmp_path=tmp_path, app_id=app_id)
        result = runner.invoke(eats, ["jobs", "run", path_to_job_env])
        assert result.exit_code == 0
        result = runner.invoke(eats, ["jobs", "wait", job_id])
        assert result.exit_code == 0
        result = runner.invoke(eats, ["jobs", "abort", job_id])
        assert result.exit_code != 0

    def test_jobs_run(self, tmp_path):
        docker_image = (
            "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_01:v3.0"
        )
        app_id = self._register_valid_app(docker_image=docker_image)
        job_id, path_to_job_env = self._register_valid_job(tmp_path=tmp_path, app_id=app_id)
        result = runner.invoke(eats, ["jobs", "run", path_to_job_env])
        assert result.exit_code == 0
        result = runner.invoke(eats, ["jobs", "wait", job_id])
        assert result.exit_code == 0
        result = runner.invoke(eats, ["jobs", "status", job_id])
        assert result.exit_code == 0
        assert "COMPLETED" in result.stdout

    def test_jobs_exec(self, tmp_path):
        docker_image = (
            "registry.gitlab.com/empaia/integration/sample-apps/v3/org-empaia-vendor_name-tutorial_app_01:v3.0"
        )
        app_id = self._register_valid_app(docker_image=docker_image)
        result = runner.invoke(eats, ["jobs", "exec", app_id, self._inputs, str(tmp_path)])
        (job_id,) = re.findall("^EMPAIA_JOB_ID=(.+)$", result.stdout, re.MULTILINE)
        assert result.exit_code == 0
        result = runner.invoke(eats, ["jobs", "status", job_id])
        assert result.exit_code == 0
        assert "COMPLETED" in result.stdout
