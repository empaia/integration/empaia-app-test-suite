import io

import docker
import pytest
from typer.testing import CliRunner

from empaia_app_test_suite.cli import app as eats

runner = CliRunner()


class ServicesUpTest:
    has_been_setup = False
    dummy_docker_image = "eats-dummy-docker:image"

    @classmethod
    def setup_class(cls):
        result = runner.invoke(eats, ["services", "wait", "--trials", "1"])
        if result.exit_code != 0:
            result = runner.invoke(eats, ["services", "up", pytest.mount_point, "--volume-prefix", "test", "--build"])
            assert result.exit_code == 0
            result = runner.invoke(eats, ["services", "wait"])
            assert result.exit_code == 0
            cls.has_been_setup = True

        docker_file = io.BytesIO("FROM scratch\nLABEL maintainer=someone@example.com\n".encode("UTF-8"))
        docker_client = docker.from_env()
        docker_client.images.build(tag=cls.dummy_docker_image, fileobj=docker_file)

    @classmethod
    def teardown_class(cls):
        if cls.has_been_setup:
            runner.invoke(eats, ["services", "down", "-v", "--volume-prefix", "test"])
        docker_client = docker.from_env()
        docker_client.images.remove(cls.dummy_docker_image)
