from time import sleep

import docker
import docker.errors
import pytest
from typer.testing import CliRunner

from empaia_app_test_suite.cli import app as eats

service_names = [
    "app-service",
    "pixelmapd",
    "annotation-service",
    "job-service",
    "examination-service",
    "clinical-data-service",
    "cds-plugin-openslide",
    "cds-plugin-tiffslide",
    "cds-plugin-tifffile",
    "cds-plugin-pil",
    "cds-plugin-wsidicom",
    "medical-data-service",
    "aaa-service-mock",
    "marketplace-service-mock",
    "job-execution-service",
    "workbench-daemon",
    "workbench-service",
    "workbench-client-v3",
    "workbench-client-v3-sample-ui",
    "workbench-client-v3-generic-ui",
    "nginx",
    "eats-postgres-db",
    "eats-mongo-db",
]

service_names_health_check = [
    "app-service",
    "medical-data-service",
    "aaa-service-mock",
    "marketplace-service-mock",
    "job-execution-service",
    "workbench-daemon",
    "workbench-service",
    "workbench-client-v3",
    "workbench-client-v3-sample-ui",
    "workbench-client-v3-generic-ui",
    "nginx",
    "eats-postgres-db",
    "eats-mongo-db",
]

cli_runner = CliRunner()


def _get_running_container_names(docker_client):
    return list(map(lambda container: container.name, docker_client.containers.list()))


def _assert_service_containers_not_running(docker_client):
    container_names = _get_running_container_names(docker_client)
    for service_name in service_names:
        assert service_name not in container_names


def _assert_service_containers_running(docker_client):
    container_names = _get_running_container_names(docker_client)
    for service_name in service_names:
        assert service_name in container_names


def _assert_services_unhealthy():
    result = cli_runner.invoke(eats, ["services", "health"])
    assert result.exit_code == 0
    for service_name in service_names_health_check:
        if "service" in service_name:
            assert f"\N{ballot X} {service_name}" in result.stdout


def _assert_services_healthy():
    result = cli_runner.invoke(eats, ["services", "health"])
    assert result.exit_code == 0
    for service_name in service_names_health_check:
        if "service" in service_name:
            assert f"\N{check mark} {service_name}" in result.stdout


def _assert_services_cannot_be_waited_for():
    result = cli_runner.invoke(eats, ["services", "wait", "--trials", "1"])
    assert result.exit_code != 0


def _assert_services_can_be_waited_for():
    result = cli_runner.invoke(eats, ["services", "wait"])
    assert result.exit_code == 0
    assert "\N{check mark} Waiting for all services to be ready" in result.stdout


def test_services_without_args():
    result = cli_runner.invoke(eats, ["services"])
    assert "Usage: eats services [OPTIONS] COMMAND [ARGS]" in result.stdout


def test_services_list():
    result = cli_runner.invoke(eats, ["services", "list"])
    assert result.exit_code == 0
    for service_name in service_names:
        assert service_name in result.stdout


def test_services_volumes():
    result = cli_runner.invoke(eats, ["services", "volumes"])
    assert result.exit_code == 0
    volume_names = [
        "eats-postgres-db-vol",
        "eats-mongo-db-vol",
        "js-rsa-vol",
        "es-rsa-vol",
        "wbs-rsa-vol",
        "marketplace-service-mock-vol",
    ]
    for volume_name in volume_names:
        assert volume_name in result.stdout
    result = cli_runner.invoke(eats, ["services", "volumes", "--volume-prefix", "test"])
    assert result.exit_code == 0
    for volume_name in volume_names:
        if "/" not in volume_name:
            assert f"test_{volume_name}" in result.stdout
        else:
            assert volume_name in result.stdout


def test_services_up_down_health_wait():
    docker_client = docker.from_env()
    _assert_service_containers_not_running(docker_client)
    _assert_services_unhealthy()
    _assert_services_cannot_be_waited_for()
    try:
        with cli_runner.isolated_filesystem():
            result = cli_runner.invoke(eats, ["services", "up", pytest.mount_point, "--volume-prefix", "test"])
            assert result.exit_code == 0

            _assert_service_containers_running(docker_client)
            sleep(10)
            _assert_services_can_be_waited_for()
            _assert_services_healthy()

            result = cli_runner.invoke(eats, ["services", "down"])
            assert result.exit_code == 0
            sleep(1)
            _assert_service_containers_not_running(docker_client)
    finally:
        cli_runner.invoke(eats, ["services", "down", "--volume-prefix", "test"])


def test_services_up_down_health_wait_changed_ports():
    docker_client = docker.from_env()
    _assert_service_containers_not_running(docker_client)
    _assert_services_unhealthy()
    _assert_services_cannot_be_waited_for()
    try:
        with cli_runner.isolated_filesystem():
            result = cli_runner.invoke(
                eats,
                [
                    "services",
                    "up",
                    pytest.mount_point,
                    "--nginx-port",
                    "9999",
                    "--volume-prefix",
                    "test",
                ],
            )
            assert result.exit_code == 0

            _assert_service_containers_running(docker_client)
            sleep(10)
            _assert_services_can_be_waited_for()
            _assert_services_healthy()

            result = cli_runner.invoke(eats, ["services", "down"])
            assert result.exit_code == 0
            sleep(1)
            _assert_service_containers_not_running(docker_client)
    finally:
        cli_runner.invoke(eats, ["services", "down", "--volume-prefix", "test"])
