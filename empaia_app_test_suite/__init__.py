from importlib.metadata import version

__version__ = version("empaia_app_test_suite")
