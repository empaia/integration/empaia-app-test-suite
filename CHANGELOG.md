# EATS Changelog

## v3.7.2

* updated services

## v3.7.1

* updated services

## v3.7.0

* updated services

## v3.6.1

* updated services

## v3.6.0

* updated all services
* updated generic app ui v3
* changed services cli in respect of wsi mount point
* changes in slide registration

## v3.5.1

* updated python dependency to >= 3.10

## v3.5.0

* added support for pixelmaps
* updated all services
* updated generic app ui v3

## v3.4.6

* enable support for mulit-file WSIs when registering a slide/job

## v3.4.5

* updated services

## v3.4.4

* updated JES and WBS/WBD

## v3.4.3

* updated App Service and MPS mock to fix data type error in configuration responses

## v3.4.2

* updated WBS

## v3.4.1

* updated MDS and WBS

## v3.4.0

* updated services and dependencies
* added multi-user support
* updated to pydantic v2

## v3.3.0

* updated services and dependencies

## v3.2.2

* increase frontend token expiration, because browser caching does not work with strict CSP

## v3.2.1

* updated services
* updated input class validation

## v3.2.0

* added Mirax-Plugin
* fixed error when running apps without JES

## v3.1.0

* updated services and dependencies

## v3.0.0

* added Workbench Client 3.0
  * new EADv3 schema
    * processing modes for apps
      * preprocessing of complete WSIs
      * standalone (ROI based processing)
      * postprocessing of previously generated results (containerized or in App UI)
* removed Workbench Client 1.0 and 2.0
* updated services and dependencies

## v2.0.23

* updated services and dependencies

## v2.0.22

* updated services and dependencies
* added aditional filter to class and primitive query

## v2.0.21

* updated services and dependencies
* adapted for changed CSP settings for app UIs

## v2.0.20

* updated services and dependencies

## v2.0.19

* updated services and dependencies

## v2.0.18

* updated services and dependencies

## v2.0.17

* updated WBD

## v2.0.16

* updated services
* fixed error in organization generation

## v2.0.15

* updated services

## v2.0.14

* updated services

## v2.0.13

* updated services
* added app-ui-config to eats apps register command
* improved output of eats slides register command

## v2.0.12

* updated services
* removed EAD-Validation-Service
* added py_ead_validation as submodule to validate EADs and app configurations

## v2.0.11

* updated services
* pinned version of mongo image to major version 5

## v2.0.10

* updated services

## v2.0.9

* updated services

## v2.0.8

* added test for GPU support
* added health check before dependant commands

## v2.0.7

* Updated services

## v2.0.6

* Enabled automatic updates with renovate
* Fixed ci release tag

## v2.0.5

* Updated WBS

## v2.0.4

* Updated CI

## v2.0.3

* Updated url to EATS repository

## v2.0.2

* Added docker pull to most services
* Changed to custom-pg image for postgres
* Added support for app-ui frame-ancestors and connect-src content-security-policies for experiments
* Added gitlab ci step to publish build package to test.pypi.org
* Updated README

## v2.0.1

* Updated custom models
* Added test for tutorial app 09
* Removed debug prints in tests

## v2.0.0

* Major update for WBC 2.0
  * Add app ui url in app register
  * WBC v1 and v2 are started in parallel
  * Added nginx as reverse-proxy
* Replaced SSS and VS by MPS and AAA mocks
* Update App Service to include Error Message Endpoint
* Using the CLI, lock inputs on jobs run and not jobs register
* Improved apps list output
* Added slides list output
* Added jobs list output
* Extended command tests
* Added better error messages

## v1.2.3

* added arg to provide docker config.json `--docker-config`
* refactored sample apps tests
* added custom postgres.conf (mds db memory optimization)
* added arg to provide docker config.json `--docker-config`
* refactored sample apps tests
* added custom postgres.conf (mds db memory optimization)

## v1.2.2

* fixed bug preventing input classes to be persisted

## v1.2.1

* fixed bug where a shell exported EMPAIA_JOB_ID variable could overwrite the variables provided by the job.env file for eats jobs run

## v1.2.0

* eats jobs set-running <job_id>
  * set job at MDS to RUNNING if the app is intended to be started natively (without JES)
* updated all service sto latest (as of 2021-11-12)
* changed to "opensourced" JES
* bugfix concerning dynamic assignment of docker container ports
* added validation when multiple wsis are used as job input with same id but different other properties.
* eats services down
  * optional: -v switch to delete all volumes aswell
* updated services
  * job-service switched from MongoDB to PSQL
  * services share a single PSQL instance
  * it is recommended to remove existing docker images before updating
* fix bug checking config file is not in inputs folder
  * new fix to only process JSON files in input folder with filename as inputs parameter OR being classes (ROI classes are not listed seperately as inputs)
* added isyntax support
  * activate by running `services up` with flag `--isyntax-sdk=<path/to/philips-pathologysdk-2.0-ubuntu18_04_py36_research.zip>` once
* fixed help for `--build` flag

## v1.1.5

* updated services
* built from open-source repo
* enabled token validation in app-service
* updated WSI-Service to fix concurrency bug

## v1.1.4

* updated models for custom validation of reference_type

## v1.1.3

* fix bug in web-client that prevented certain slides of being displayed

## v1.1.2

* Exceptions do not leave the CLI scope anymore but are handled and printed accordingly.
* Make use of CLI framework's echo method instead of pure print.
* Update of internal services due to interface changes
* Update Web-Client

## v1.1.1

* app-service implementation prevented changes from 1.1.0 of derived-annotation-data-service to be effective. fixed in new app-service 0.3.6

## v1.1.0

* bug fix for derived-annotation-data-service
  * posting data lists now returns the list elements in the same order

## v1.0.0

* Reworked CLI
* Web-Client integration
* Updated Annotation Models
  * required: npp_created
  * required: reference_type
  * optional: npp_viewing
  * optional: centroid
