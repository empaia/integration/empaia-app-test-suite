import json
import sys
from urllib import request

from packaging import version
from pkg_resources import parse_version


def get_published_versions():
    url = "https://test.pypi.org/pypi/empaia-app-test-suite/json"
    releases = json.loads(request.urlopen(url).read())["releases"]
    return sorted(releases, key=parse_version, reverse=True)


if __name__ == "__main__":
    new_version = sys.argv[1]
    published_versions = get_published_versions()
    sys.tracebacklimit = -1
    try:
        assert new_version not in published_versions
    except AssertionError as e:
        print(f"Version {new_version} already published!")
        print(f"Published Versions: {published_versions}")
        raise e

    try:
        if len(published_versions) > 0:
            assert version.parse(new_version) > version.parse(published_versions[0])
    except AssertionError as e:
        print(f"Version {new_version} not greater than latest published version {published_versions[0]}!")
        print(f"Published Versions: {published_versions}")
        raise e
